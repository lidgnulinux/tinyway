# Tinywl versi lanjut

## For English README, see [ENGLISH README](https://gitlab.com/lidgl/tinyway/-/blob/main/README-eng.md).

## Apa ini ?

### Tinywl dengan beberapa fitur tambahan. 

Fitur tambahan di antaranya :

<details>
  <summary>Fitur Tambahan.</summary>

- Fitur tap to click, untuk pengguna touchpad.
- Fungsi close / killclient lewat keybind.
- fungsi manipulasi jendela lewat keybind.
    - move ke kiri, kanan, atas, bawah dan tengah.
    - resize ke kiri, kanan, atas, bawah dan setengah tinggi dan lebar layar.
    - maximize jendela.
    - snap jendela ke pojok kiri, kanan, atas, bawah.
    - pseudo minimize.
    - fullscreen jendela.
    - _one-third split_ layout, layout layar dibagi 3.
- launcher (wofi).
- Protokol screencopy untuk mendukung screenshot.
- Protokol primary selection.
- Fitur warna background.
- Fungsi berpindah ke VT / virtual terminal.
- Zone eksklusif untuk menaruh bar (atas / bawah, bisa di-toggle).
- Rule untuk jendela dengan _app_id_ / _title_ yang spesifik (contoh: lxqt-panel, imv).
- Tangkapan layar / _screenshot_ (melalui _tool_ eksternal grim), baik layar penuh / _fullscreen_, maupun jendela terfokus.
- _Show desktop_ sederhana.
- _Send to desktop_, mengirim jendela yang di-_minimize_ untuk muncul ke permukaan.
- Opsi untuk tema kursor, kita bisa mengatur tema kursor dengan _env_ **XCURSOR_THEME** (tema kursor) dan **XCURSOR_SIZE** (ukuran kursor).
- Dukungan _maximize_ dan _minimize_ jendela lewat tombol dekorasi.
- Protokol sederhana untuk mendukung tool yang membutuhkan protokol keyboard virtual (contoh: wtype). 
- Popup unconstrain sederhana, baru berjalan pada jendela _maximize_.

</details>

Secara default shortcut yang tersedia :

<details>
  <summary>Shortcut.</summary>

- mod + enter : membuka terminal (foot).
- mod + p : membuka launcher (wofi).
- mod + h : maximize jendela arah vertikal ke sebelah kiri.
- mod + l : maximize jendela arah vertikal ke sebelah kanan.
- mod + f : maximize jendela.
- mod + shift + f : fullscreen jendela.
- mod + esc : keluar / quit kompositor.
- mod + q : menutup / close jendela.
- mod + j : mengirim jendela ke posisi paling bawah.
- mod + k : mengirim jendela ke posisi paling atas.
- mod + {left, right, up, down} : memindah / move jendela ke arah kiri, kanan, atas dan bawah.
- mod + shift + {l, h, k, j} : resize ukuran jendela ke arah kanan, kiri, atas, dan bawah.
- mod + shift + {r, e, t, w} : memindah jendela ke arah pojok kanan, kiri, atas, dan bawah.
- mod + Tab : cycle window.
- mod + {F1,F2,..F5} : berpindah ke virtual terminal 1 sampai 5.
- mod + z : toggle zona eksklusif.
- mod + shift + z : toggle posisi zona eksklusif (atas / bawah).
- mod + w : screenshot jendela terfokus.
- mod + shift + p : screenshot layar penuh / _fullscreen_.
- mod + {1, 2, 3} : resize jendela menjadi sepertiga layar dan ditaruh ke bagian kiri, tengah, kanan.
- Logo + shift + j : Minimize semua jendela / _show desktop_.
- Logo + shift + k : mengirim jendela yang di-_minimize_ untuk muncul ke permukaan.

</details>

## Dependensi.

TinyWay memiliki dependensi pada pustaka / _library_ `wlroots` versi commit [0de36596](https://gitlab.freedesktop.org/wlroots/wlroots/-/commit/0de36596).

## Cara *build*, *install* dan *run*.

Berikut adalah cara untuk *build*, menginstall dan *run* TinyWay :

<details>
  <summary>Klik untuk expand.</summary>

1. Sesuaikan pengaturan / konfigurasi di `config_tinywl.h` !
1. Sesuaikan keybind / shortcut di `tinywl.c` pada bagian fungsi `handle_keybinding` (opsional) ! 
1. *Build* dengan perintah `make` !
1. *Install* dengan perintah `make install` ! Tinywl-next akan terinstall di `$HOME/.local/bin/`.
1. TinyWay bisa langsung dijalankan dengan perintah `$HOME/.local/bin/tinywl`.
1. Agar fitur screenshot bisa digunakan, jalankan TinyWay dengan perintah berikut !

    ```
    $ $HOME/.local/bin/tinywl > ~/.cache/tiny_info
    ```

1. Jangan lupa untuk menyalin contoh skrip _screenshot_ "misc/ss_window" ke $PATH !
1. Kita bisa menjalankan TinyWay dengan tema kursor kustom dengan perintah berikut.

    ```
    $ XCURSOR_THEME=your_cursor_theme XCURSOR_SIZE=cursor_size $HOME/.local/bin/tinywl > ~/.cache/tiny_info
    ```


</details>

## Issue dan bug.

Ada beberapa issue dan bug yang kami temui di antaranya :

<details>
  <summary>Klik untuk expand.</summary>

- Beberapa jendela tidak bisa di-resize dengan cursor / non-keybind dari pojok dan bagian atas, bawah, kanan, dan kiri (mpv dan imv).

    solusi : 

    - mpv : 
        - mendisable on-screen-controller dengan opsi `--no-osc`, bisa juga dengan menekan tombol delete.
        - resize dari samping kanan bagian tengah.

    - imv : belum kami temukan.

- Bar / panel dan wallpaper setter tidak berjalan. Ini terjadi karena kami belum menerapkan layer shell protocol.

    solusi :

    - Untuk bar / panel, bisa menggunakan lxqt-panel dan ditaruh di pojok atas.
    - Untuk wallpaper masih dalam pengembangan, untuk sementara bisa memakai pcmanfm-qt untuk mengatur wallpaper.
    - Untuk menjalankan lxqt-panel sebagai panel dan pcmanfm-qt sebagai wallpaper setter :

    ```
    $ QT_QPA_PLATFORM=wayland /usr/bin/lxqt-panel --qwindowtitle lxqt-panel 
    $ QT_QPA_PLATFORM=wayland /usr/bin/pcmanfm-qt --desktop --qwindowtitle wallpaper
    ```

- _Popup unconstrain_ belum diterapkan, menyebabkan jendela _popup_ kadang muncul di luar layar.

</details>
